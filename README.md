# RustAco

A simple Rust simulator based on my other project [PyGAco](https://www.gitlab.com/lostpy/pygaco), a simple Python Ant simulator.


## Install and Run

### Requirements

To run the simulation, [rust toolchain](https://www.rust-lang.org/) must be installed.

### Install

 * Using git:

```
git clone https://gitlab.com/LostPy/rust-ant-simulator.git
```

### Run

 * Using Cargo (Rust), execute following command in the `rust-ant-simulator` directory:

```
cargo run --release
```

## Configuration

You can customize simulation with a TOML file located in same directory that executable (with `cargo run`: the parent directory of project).

At the start of simulation, if there is not configuration file `rustaco.toml`, the file is created with default values. You can modify this file and to update the config, just restart the simulation (if the simulation running, juste press <kbd>Escape</kbd>).

### Configuration details

> **All fields are optional**

> :warning: **Map are unimplemented at the moment**.

|Field name|Type|Description|Default|
|:--------:|:--:|-----------|:-----:|
|`application`|`object`|All settings related to application (windows...).||
|`application.window-width`|`u16`|The width of window, ignored if a map is used.|`800`|
|`application.window-height`|`u16`|The height of window, ignored if a map is used.|`600`|
|`map`|`object`|All settings related to the map.||
|`map.type`|`"none"`, `"generated"` or `"tiled"`|Type of map: `none` for "no map", `generated` to generated a map at the start (unimplemented) and `tiled` to use a tiled map (unimplemented)|`"none"`|
|`map.generator`|`object`|All setting related to generator of map.||
|`map.generator.tile-size`|`u8`|Size of a tile in pixel (a tile is a square).|`16`|
|`map.generator.width`|`u8`|Number of tiles in the width.|`50`|
|`map.generator.height`|`u8`|Number of tiles in the height.|`31`|
|`map.generator.init-fill-rate`|`f32`|The initial fill rate of map (before smoothing iterations).|`0.7`|
|`map.generator.number-iterations`|`u8`|The number of smoothing iterations to do.|`2`|
|`simulation`|`object`|All settings directly related to simulation.||
|`simulation.time-update-pheromone`|`f32`|Time between 2 updates of pheromones (can be use to improve simulation, but decrease performance -> + RAM consumption)|Depends of ant count|
|`simulation.ant-count`|`u16`|Number of ant, a maximum is set to 3000|`30`|
|`simulation.ant`|`object`|All settings for ants.||
|`simulation.ant.speed`|`u8`|The speed of ants (pixel / second).|`120`|
|`simulation.ant.angle-view`|`u16`|The angle of the ants' field of view (in degree).|`90`|
|`simulation.ant.distance-view`|`u8`|The distance of view (in pixel).|`150`|

### Example

```toml
[application]
window-width = 950
window-height = 700

[simulation]
ant-count = 200

[simulation.ant]
speed = 100
distance-view = 80
```

## Todo

 * Documentation
 * Simulation:
   - Add Map generation (create an crate to generate 2D map)
   - Implement collision with walls


## Demo

![example](im/example.mp4)

