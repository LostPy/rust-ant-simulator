use std::path::Path;

use macroquad::prelude::*;
use macroquad::color::Color;

use rustaco::config::MapType;
use rustaco::errors::SimulationError;
use rustaco::event::{Event, EventHandler};
use rustaco::{SimulationState, Simulation, Config};
use rustaco::entities::Map;


const BACKGROUND_COLOR: Color = Color::new(0.17, 0.20, 0.25, 1.);


#[macroquad::main("Rust Ant Simulator")]
async fn main() {
    let config_path = &Path::new("./rustaco.toml");
    let config = match Config::new(&config_path) {
        Ok(config) => config,
        Err(err) => match err {
            SimulationError::SettingsDeError(err) => panic!(
                "Error in config file 'rustaco.toml': {:?}", err),
            SimulationError::CantReadSettings(_) => {
                let config = Config::default();
                config.save(&config_path).ok();
                config
            },
            _ => unreachable!(),
        },
    };

    // Set Windows size
    match config.map.map_type() {
        MapType::None => request_new_screen_size(
            config.app.window_width() as f32,
            config.app.window_height() as f32,
        ),
        MapType::Generated => todo!(),
        MapType::Tiled => todo!(),
    }

    let mut simulation = Simulation::new(config);

    loop {
        clear_background(BACKGROUND_COLOR);

        // handle events
        let events = get_events(simulation.state, &simulation.map);
        for event in events {
            simulation.on_event(event).unwrap();
        }

        // Update entities
        match simulation.state {
            SimulationState::Playing => simulation.update(),
            _ => {},
        }

        // draw
        simulation.draw();
        next_frame().await
    }
}


fn get_events(state: SimulationState, map: &Map) -> Vec<Event> {
    let mut events: Vec<Event> = Vec::new();
    let mouse_pos = mouse_position();
    match state {
        SimulationState::Playing => {
            if is_key_pressed(KeyCode::Space) {
                events.push(Event::Pause);
            }

            if is_mouse_button_pressed(MouseButton::Left) {
                events.push(Event::AddFood(mouse_pos.0, mouse_pos.1));
            }
        },
        SimulationState::Pause => {
            if is_key_pressed(KeyCode::Space) {
                events.push(Event::Resume);
            }

            if is_mouse_button_pressed(MouseButton::Left) {
                events.push(Event::AddSourceFood(mouse_pos.0, mouse_pos.1));
            }
        },
        SimulationState::Initialisation => {
            if is_key_pressed(KeyCode::Space) {
                events.push(Event::Resume);
            }

            if is_mouse_button_pressed(MouseButton::Left) {
                match map.colony {
                    Some(_) => events.push(Event::AddSourceFood(mouse_pos.0, mouse_pos.1)),
                    None => events.push(Event::AddColony(mouse_pos.0, mouse_pos.1)),
                }
            }
        },
    }

    if is_key_pressed(KeyCode::Escape) {
        events.push(Event::Reset);
    }
    events
}

