use std::f32;
use serde_derive::{Deserialize, Serialize};


#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct AntConfig {
    /// Speed of ants in pixel / second
    #[serde(default = "AntConfig::default_speed")]
    speed: u8,
    /// Distance of view in pixel
    #[serde(default = "AntConfig::default_distance_view")]
    distance_view: u8,
    /// Angle of view in degrees
    #[serde(default = "AntConfig::default_angle_view")]
    angle_view: u16,
}

impl Default for AntConfig {
    fn default() -> Self {
        Self {
            speed: Self::default_speed(),
            distance_view: Self::default_distance_view(),
            angle_view: Self::default_angle_view(),
        }
    }
}

impl AntConfig {

    pub fn speed(&self) -> u8 {
        self.speed
    }

    pub fn distance_view(&self) -> u8 {
        self.distance_view
    }

    pub fn angle_view(&self) -> f32 {
        self.angle_view as f32 * f32::consts::PI / 180f32
    }

    const fn default_speed() -> u8 {
        120
    }

    const fn default_distance_view() -> u8 {
        150
    }

    const fn default_angle_view() -> u16 {
        90
    }
}


#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", default)]
pub struct SimulationConfig {
    #[serde(rename = "time-update-pheromone")]
    time_update_phero: Option<f32>,
    #[serde(default = "SimulationConfig::default_ant_count")]
    ant_count: u16,
    ant: AntConfig,
}


impl Default for SimulationConfig {
    fn default() -> Self {
        Self {
            time_update_phero: None,
            ant_count: Self::default_ant_count(),
            ant: AntConfig::default(),
        }
    }
}

impl SimulationConfig {
    pub fn time_update_phero(&self) -> Option<f32> {
        self.time_update_phero
    }

    pub fn ant_count(&self) -> u16 {
        self.ant_count
    }

    pub fn ant(&self) -> &AntConfig {
        &self.ant
    }

    const fn default_ant_count() -> u16 {
        30
    }
}


#[cfg(test)]
mod tests {
    use super::{AntConfig, SimulationConfig};
    
    #[test]
    fn test_ant_config() {
        let string = "\
speed = 100
distance-view = 120
";
        let config: AntConfig = toml::from_str(string).unwrap();
        assert_eq!(toml::to_string(&config).unwrap(),"\
speed = 100
distance-view = 120
angle-view = 90
");
    }

    #[test]
    fn test_simu_config() {
        let string = "\
[ant]
speed = 100
distance-view = 120
";
        let config: SimulationConfig = toml::from_str(string).unwrap();
        assert_eq!(toml::to_string(&config).unwrap(), "\
ant-count = 30

[ant]
speed = 100
distance-view = 120
angle-view = 90
");
    }
}
