use serde_derive::{Deserialize, Serialize};


#[derive(Debug, Deserialize, Serialize, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum MapType {
    None,
    Generated,
    Tiled,
}

impl Default for MapType {
    fn default() -> Self {
        Self::None
    }
}


#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct GeneratorConfig {
    /// Size of tiles (in pixel)
    #[serde(default = "GeneratorConfig::default_tile_size")]
    tile_size: u8,
    /// Number of tiles on the width
    #[serde(default = "GeneratorConfig::default_width")]
    width: u8,
    /// Number of tiles on the height.
    #[serde(default = "GeneratorConfig::default_height")]
    height: u8,
    /// The fill rate at the initialization of generator
    #[serde(default = "GeneratorConfig::default_fill_rate")]
    init_fill_rate: f32,
    #[serde(default = "GeneratorConfig::default_nb_iter")]
    number_iterations: u8,
}

impl Default for GeneratorConfig {
    fn default() -> Self {
        Self {
            tile_size: Self::default_tile_size(),
            width: Self::default_width(),
            height: Self::default_height(),
            init_fill_rate: Self::default_fill_rate(),
            number_iterations: Self::default_nb_iter(),
        }
    }
}

impl GeneratorConfig {
    pub fn tile_size(&self) -> u8 {
        self.tile_size
    }

    pub fn width(&self) -> u8 {
        self.width
    }

    pub fn height(&self) -> u8 {
        self.height
    }

    pub fn width_to_pixels(&self) -> u16 {
        self.width as u16 * self.tile_size as u16
    }

    pub fn height_to_pixels(&self) -> u16 {
        self.height as u16 * self.tile_size as u16
    }

    const fn default_tile_size() -> u8 {
        16
    }

    const fn default_width() -> u8 {
        50
    }

    const fn default_height() -> u8 {
        31
    }

    const fn default_fill_rate() -> f32 {
        0.7
    }

    const fn default_nb_iter() -> u8 {
        2
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(default)]
pub struct MapConfig {
    #[serde(rename = "type")]
    map_type: MapType,
    generator: GeneratorConfig,
}

impl Default for MapConfig {
    fn default() -> Self {
        Self {
            map_type: MapType::default(),
            generator: GeneratorConfig::default(),
        }
    }
}

impl MapConfig {
    pub fn map_type(&self) -> MapType {
        self.map_type
    }

    pub fn generator(&self) -> &GeneratorConfig {
        &self.generator
    }
}


#[cfg(test)]
mod tests {
    use super::{GeneratorConfig, MapConfig, MapType};

    #[test]
    fn test_generator_config() {
        let string = "\
tile-size = 32
width = 25
height = 16";

        let config: GeneratorConfig = toml::from_str(string).unwrap();
        let serialized = toml::to_string(&config).unwrap();
        assert_eq!(serialized, "\
tile-size = 32
width = 25
height = 16
init-fill-rate = 0.7
number-iterations = 2
");
    }

    #[test]
    fn test_map_config() {
        let string = "\
type = \"generated\"

[generator]
tile-size = 32
width = 25
height = 16";

        let config: MapConfig = toml::from_str(string).unwrap();
        match config.map_type() {
            MapType::Generated => assert!(true, "map type: Generated"),
            _ => assert!(false, "map type: not generated"),
        }

        let serialized = toml::to_string(&config).unwrap();
        assert_eq!(serialized, "\
type = \"generated\"

[generator]
tile-size = 32
width = 25
height = 16
init-fill-rate = 0.7
number-iterations = 2
");
    }
}
