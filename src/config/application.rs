use serde_derive::{Serialize, Deserialize};


#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ApplicationConfig {
    window_width: u16,
    window_height: u16,
}

impl Default for ApplicationConfig {
    fn default() -> Self {
        Self {window_width: 800, window_height: 600}
    }
}

impl ApplicationConfig {
    pub fn window_width(&self) -> u16 {
        self.window_width
    }

    pub fn window_height(&self) -> u16 {
        self.window_height
    }
}


#[cfg(test)]
mod tests {
    use super::ApplicationConfig;

    #[test]
    fn test_app_config() {
        let valid_result = "\
window-width = 900
window-height = 500
";
        let config = ApplicationConfig {
            window_width: 900,
            window_height: 500,
        };
        let serialized_config: ApplicationConfig = toml::from_str(valid_result).unwrap();

        assert_eq!(config.window_width(), 900);
        assert_eq!(config.window_height(), 500);
        assert_eq!(toml::to_string(&config).unwrap(), valid_result);
        assert_eq!(serialized_config.window_width(), 900);
        assert_eq!(serialized_config.window_height(), 500);
    }
}

