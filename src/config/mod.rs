use std::{
    path::Path,
    fs,
};

use serde_derive::{Deserialize, Serialize};

use crate::errors::{SimulationResult, SimulationError};

mod application;
mod map;
mod simulation;

pub use application::ApplicationConfig;
pub use map::{GeneratorConfig, MapType, MapConfig};
pub use simulation::{AntConfig, SimulationConfig};


#[derive(Default, Debug, Deserialize, Serialize)]
#[serde(default)]
pub struct Config {
    #[serde(rename = "application")]
    pub app: ApplicationConfig,
    pub simulation: SimulationConfig,
    pub map: MapConfig,
}

impl Config {
    /// Create a Config instance from a path (to a config file).
    pub fn new(path: &Path) -> SimulationResult<Self> {
        let string_config = match fs::read_to_string(path) {
            Ok(string) => string,
            Err(err) => return Err(SimulationError::CantReadSettings(err)),
        };
        Self::from_str(&string_config)
    }

    /// Create a Config instance from a `&str`.
    pub fn from_str(string: &str) -> SimulationResult<Self> {
        match toml::from_str(string) {
            Ok(config) => Ok(config),
            Err(err) => Err(SimulationError::SettingsDeError(err)),
        }
    }

    /// Create a Config instance from a String.
    pub fn from_string(string: String) -> SimulationResult<Self> {
        Self::from_str(&string)
    }

    /// Serialize as a string with the TOML format.
    pub fn to_string(&self) -> SimulationResult<String> {
        match toml::to_string(self) {
            Ok(string) => Ok(string),
            Err(err) => Err(SimulationError::SettingsSerError(err)),
        }
    }

    /// Save config in a file (TOML).
    pub fn save(&self, path: &Path) -> SimulationResult<()> {
        let string_config = self.to_string()?;

        if let Err(err) = fs::write(path, string_config) {
            Err(SimulationError::CantWriteSettings(err))
        } else {
            Ok(())
        }
    }
}


#[cfg(test)]
mod tests {
    use super::Config;

    #[test]
    fn test_config() {
        let string = "\
[application]
window-width = 900
window-height = 500

[simulation]
time-update-pheromone = 3.0
ant-count = 50
ant = { speed = 100, distance-view = 120 }
";
    let config = Config::from_str(string).unwrap();
    assert_eq!(toml::to_string(&config).unwrap(), "\
[application]
window-width = 900
window-height = 500

[simulation]
time-update-pheromone = 3.0
ant-count = 50

[simulation.ant]
speed = 100
distance-view = 120
angle-view = 90

[map]
type = \"none\"

[map.generator]
tile-size = 16
width = 50
height = 31
init-fill-rate = 0.7
number-iterations = 2
");
    }
}
