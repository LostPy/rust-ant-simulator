
#[derive(Debug)]
pub enum SimulationError {
    ColonyAlreadyExistError,
    ColonyNotExisting,
    TooManyAnts,
    SettingsDeError(toml::de::Error),
    SettingsSerError(toml::ser::Error),
    CantReadSettings(std::io::Error),
    CantWriteSettings(std::io::Error),
}

pub type SimulationResult<T> = Result<T, SimulationError>;

