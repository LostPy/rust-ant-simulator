use std::path::Path;

use macroquad::time;

use crate::Config;
use crate::entities::{Entity, Map, Ant, Drawable, Updateable, Colony};
use crate::errors::{SimulationResult, SimulationError};
use crate::event::EventHandler;


#[derive(Clone, Copy)]
pub enum SimulationState {
    Playing,
    Pause,
    Initialisation,
}


pub struct Simulation {
    pub state: SimulationState,
    pub map: Map,
    pub config: Config,
    ants: Vec<Ant>,
    /// Elapsed time in second.
    elapsed_time: f32,
    /// Last update pheromones timer
    last_update_pheromones: f32,
    delta_update_phero: f32,
}

impl Simulation {

    pub fn new(config: Config) -> Self {
        Self {
            config,
            ..Default::default()
        }
    }

    pub fn setup(&mut self) {
        self.state = SimulationState::Initialisation;
        self.ants = Vec::new();
        self.map = Map::default();
        self.elapsed_time = 0f32;
        self.last_update_pheromones = 0f32;
        Ant::setup(self.config.simulation.ant());
        if let Ok(config) = Config::new(&Path::new("./rustaco.toml")) {
            self.config = config;
        };
    }

    pub fn update(&mut self) {
        let time_delta = time::get_frame_time();
        self.elapsed_time += time_delta;

        if self.elapsed_time - self.last_update_pheromones > self.delta_update_phero {
            self.map.update(time_delta, Some(self.delta_update_phero), &self.config);  // update pheromones
            self.last_update_pheromones = self.elapsed_time;
        } else {
            self.map.update(time_delta, None, &self.config);  // no update pheromones
        }

        for ant in self.ants.iter_mut() {
            ant.update(time_delta, &mut self.map, &self.config);
            match ant.food() {
                Some(_) => ant.reached_colony(&self.map.colony),
                None => ant.reached_food(&mut self.map.foods),
            }
            if self.elapsed_time == self.last_update_pheromones {
                // pheromones updated
                self.map.add_pheromone(ant.x(),
                                       ant.y(),
                                       ant.get_internal_clock(),
                                       ant.get_pheromone_type());
            }
        }
    }

    pub fn draw(&self) {
        self.map.draw();
        for ant in self.ants.iter() {
            ant.draw();
        }
    }

    pub fn add_ant(&mut self) -> SimulationResult<()>{
        match self.map.colony.as_ref() {
            Some(colony) => self.ants.push(Ant::new(&colony.borrow())),
            None => return Err(SimulationError::ColonyNotExisting),
        }
        Ok(())
    }

    pub fn setup_ants(&mut self, nb_ants: u16) -> SimulationResult<()> {
        if nb_ants > 3000 {
            return Err(SimulationError::TooManyAnts);
        }

        for _ in 0..nb_ants {
            self.add_ant()?;
        }

        self.delta_update_phero = match self.config.simulation.time_update_phero() {
            Some(delta_update) => delta_update,
            None => if nb_ants <= 100 {
                1f32
            } else if nb_ants <= 300 {
                2f32
            } else if nb_ants <= 500 {
                4f32
            } else if nb_ants <= 800 {
                6f32
            } else {
                10f32
            },
        };
        Ok(())
    }
}

impl Default for Simulation {
    fn default() -> Self {
        Self {
            state: SimulationState::Initialisation,
            map: Map::default(),
            ants: Vec::new(),
            elapsed_time: 0f32,
            last_update_pheromones: 0f32,
            delta_update_phero: 1f32,
            config: Config::default(),
        }
    }
}

impl EventHandler for Simulation {
    fn on_pause(&mut self) -> SimulationResult<()> {
        self.state = SimulationState::Pause;
        Ok(())
    }


    fn on_resume(&mut self) -> SimulationResult<()> {
        match self.state {
            SimulationState::Initialisation => self.setup_ants(
                self.config.simulation.ant_count())?,
            _ => {},
        }
        self.state = SimulationState::Playing;
        Ok(())
    }

    fn on_reset(&mut self) -> SimulationResult<()> {
        self.setup();
        Ok(())
    }

    fn on_add_colony(&mut self, x: f32, y: f32) -> SimulationResult<()> {
        self.map.set_colony(Colony::new(x, y))
    }

    fn on_add_food(&mut self, x: f32, y: f32) -> SimulationResult<()> {
        self.map.add_food(x, y);
        Ok(())
    }

    fn on_add_source_food(&mut self, x: f32, y: f32) -> SimulationResult<()> {
        self.map.add_source_food(x, y);
        Ok(())
    }
}

