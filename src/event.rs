use crate::errors::SimulationResult;


pub enum Event {
    /// Event to set the simulation state to Pause state from Playing state.
    Pause,
    /// Event to set the simulation state to Playing state 
    /// from Pause state or Initialisation state.
    Resume,
    /// Event to reset the simulation.
    Reset,
    /// Event to add a source food to the mouse position.
    AddSourceFood(f32, f32),
    /// Event to add a colony to the mouse position.
    AddColony(f32, f32),
    /// Event to add a single food to the mouse position.
    AddFood(f32, f32),
}


pub trait EventHandler {
    fn on_pause(&mut self) -> SimulationResult<()> {Ok(())}
    fn on_resume(&mut self) -> SimulationResult<()> {Ok(())}
    fn on_reset(&mut self) -> SimulationResult<()> {Ok(())}
    fn on_add_source_food(&mut self, _x: f32, _y: f32) -> SimulationResult<()> {Ok(())}
    fn on_add_colony(&mut self, _x: f32, _y: f32) -> SimulationResult<()> {Ok(())}
    fn on_add_food(&mut self, _x: f32, _y: f32) -> SimulationResult<()> {Ok(())}

    fn on_event(&mut self, event: Event) -> SimulationResult<()> {
        match event {
            Event::Pause => self.on_pause(),
            Event::Resume => self.on_resume(),
            Event::Reset => self.on_reset(),
            Event::AddSourceFood(x, y) => self.on_add_source_food(x, y),
            Event::AddColony(x, y) => self.on_add_colony(x, y),
            Event::AddFood(x, y) => self.on_add_food(x, y),
        }
    }
}

