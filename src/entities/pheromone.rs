use macroquad::prelude::{Color, Vec2};
use macroquad::shapes::draw_circle;
use crate::Config;
use crate::constants::{PHEROMONE_SIZE, PHEROMONE_COLORS};

use super::{Entity, Drawable, Updateable};


#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PheromoneType {
    /// Pheromone use when the ants is not carrying food.
    /// Pheromone to show the path to the colony.
    Colony,
    /// Pheromone use when the ants carry food.
    /// Pheromone to show the path to a source foods.
    Food,
}

impl PheromoneType {
    pub fn coeff_intensity(&self) -> f32 {
        match self {
            Self::Colony => 150f32,
            Self::Food => 50f32,
        }
    }
}

impl PheromoneType {
    pub fn new(has_food: bool) -> Self {
        if has_food {
            return Self::Food;
        }
        Self::Colony
    }
}


#[derive(Debug, Copy, Clone)]
pub struct Pheromone {
    pheromone_type: PheromoneType,
    intensity: u16,
    position: Vec2,
}

impl Pheromone {

    pub fn new(x: f32, y: f32, pheromone_type: PheromoneType, time: f32) -> Self {
        Self {
            pheromone_type,
            position: Vec2::new(x, y),
            intensity: Self::calcul_intensity(time, pheromone_type),
        }
    }

    pub fn calcul_intensity(time: f32, phero_type: PheromoneType) -> u16 {
        (phero_type.coeff_intensity() * f32::exp(-time / 10.)) as u16
    }

    pub fn size() -> f32 {
        PHEROMONE_SIZE
    }

    pub fn radius() -> f32 {
        PHEROMONE_SIZE / 2.
    }

    pub fn get_type(&self) -> PheromoneType {
        self.pheromone_type
    }

    pub fn get_intensity(&self) -> u16 {
        self.intensity
    }
}

impl Entity<Self> for Pheromone {
    fn x(&self) -> f32 {
        self.position.x
    }

    fn y(&self) -> f32 {
        self.position.y
    }
}

impl Drawable for Pheromone {
    fn draw(&self) {
        let color: Color = match self.pheromone_type {
            PheromoneType::Colony => PHEROMONE_COLORS.0,
            PheromoneType::Food => PHEROMONE_COLORS.1,
        };
        draw_circle(self.position.x, self.position.y, Pheromone::radius(), color);
    }
}

impl Updateable for Pheromone {
    fn update(&mut self, time_delta: f32, _map: &super::Map, _config: &Config) {
        let intensity_to_remove = 2 * time_delta as u16;
        if self.intensity as i32 - intensity_to_remove as i32 >= 0 {
            self.intensity -= intensity_to_remove;
        } else {
            self.intensity = 0;
        }
    }
}

impl PartialEq for Pheromone {
    fn eq(&self, other: &Pheromone) -> bool {
        std::ptr::eq(self, other)
    }
}

