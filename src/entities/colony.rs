use macroquad::{shapes::draw_circle, text::draw_text, math::Vec2};

use crate::constants::{COLONY_SIZE, COLONY_COLOR, TEXT_COLOR};

use super::{Entity, Drawable, Updateable};


#[derive(Debug)]
pub struct Colony {
    position: Vec2,
    pub colected_foods: usize,
}

impl Colony {
    pub fn new(x: f32, y: f32) -> Self {
        Self {position: Vec2::new(x, y), colected_foods: 0}
    }

    pub fn size() -> f32 {
        COLONY_SIZE
    }

    pub fn radius() -> f32 {
        COLONY_SIZE / 2.
    }
}

impl Entity<Self> for Colony {
    fn x(&self) -> f32 {
        self.position.x
    }

    fn y(&self) -> f32 {
        self.position.y
    }
}

impl Drawable for Colony {
    fn draw(&self) {
        draw_circle(self.position.x, self.position.y, Colony::radius(), COLONY_COLOR);
        draw_text(&format!("{}", self.colected_foods),
                  self.position.x - 10f32,
                  self.position.y,
                  30f32,
                  TEXT_COLOR);
    }
}

impl Updateable for Colony {}

