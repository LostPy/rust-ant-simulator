/// Module to group all entities.
mod colony;
mod food;
mod pheromone;
mod map;
mod ant;


pub use colony::Colony;
pub use ant::Ant;
pub use pheromone::{Pheromone, PheromoneType};
pub use food::Food;
pub use map::Map;

use crate::Config;


pub trait Updateable {
    fn update(&mut self, _time_delta: f32, _map: &Map, _config: &Config) {}
}

pub trait Drawable {
    fn draw(&self);
}

pub trait Entity<T: Updateable + Drawable> {
    fn x(&self) -> f32;
    fn y(&self) -> f32;
}

