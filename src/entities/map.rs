use std::cell::RefCell;
use macroquad::rand;

use crate::{errors::{SimulationResult, SimulationError}, Config};

use super::{
    Colony,
    Food,
    Pheromone,
    Drawable,
    Updateable,
    PheromoneType
};


pub struct Map {
    pub colony: Option<RefCell<Colony>>,
    pub foods: Vec<RefCell<Food>>,
    pub pheromones: RefCell<Vec<RefCell<Pheromone>>>,  // See for a best way whithout `RefCell` on `Vec`.
}


impl Map {
    pub fn set_colony(&mut self, colony: Colony) -> SimulationResult<()> {
        match self.colony {
            Some(_) => return Err(SimulationError::ColonyAlreadyExistError),
            None => self.colony = Some(RefCell::new(colony)),
        }
        Ok(())
    }

    pub fn has_colony(&self) -> bool {
        self.colony.is_some()
    }

    pub fn add_food(&mut self, x: f32, y: f32) {
        self.foods.push(RefCell::new(Food::new(x, y)));
    }

    pub fn add_source_food(&mut self, x: f32, y: f32) {
        let radius: f32 = rand::gen_range(15., 35.);
        let number: u16 = rand::gen_range(radius * 2., radius * 3.) as u16;
        for _ in 0..number {
            self.add_food(
                rand::gen_range(x - radius, x + radius),
                rand::gen_range(y - radius, y + radius)
            );
        }
    }

    pub fn add_pheromone(&mut self, x: f32, y: f32, time: f32, type_: PheromoneType) {
        self.pheromones
            .borrow_mut()
            .push(RefCell::new(Pheromone::new(x, y, type_, time)));
    }

    pub fn update(&self, time_delta: f32, time_delta_phero: Option<f32>, config: &Config) {
        if let Some(col) = self.colony.as_ref() {
            col.borrow_mut().update(time_delta, self, config);
        }
        if let Some(time_delta_phero) = time_delta_phero {
            self.pheromones.borrow()
                .iter()
                .for_each(|pheromone| pheromone.borrow_mut().update(
                            time_delta_phero, &self, config));
            self.pheromones.borrow_mut().retain(
                |pheromone| pheromone.borrow().get_intensity() > 0
            );
        }
    }
}


impl Default for Map {
    fn default() -> Self {
        Self {colony: None, foods: Vec::new(), pheromones: RefCell::new(Vec::new())}
    }
}

impl Drawable for Map {
    fn draw(&self) {
        for food in self.foods.iter() {
            food.borrow().draw();
        }

        for pheromone in self.pheromones.borrow().iter() {
            pheromone.borrow().draw();
        }

        if let Some(colony) = self.colony.as_ref() {
            colony.borrow().draw();
        }
    }
}

