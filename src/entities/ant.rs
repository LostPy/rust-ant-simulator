use std::f32;
use std::cell::RefCell;
use std::sync::Mutex;

use once_cell::sync::Lazy;
use macroquad::{
    shapes::draw_triangle,
    prelude::{
        Vec2,
        screen_width,
        screen_height,
    },
    rand
};

use crate::{
    constants::{
        ANT_COLOR,
        ANT_WIDTH,
        ANT_HEIGHT,
        COLONY_SIZE,
    },
    config::{Config, AntConfig},
};

use super::{
    Colony,
    Entity,
    Drawable,
    Updateable,
    Pheromone,
    PheromoneType,
    Food,
    Map,
};


static SPEED: Lazy<Mutex<u8>> = Lazy::new(|| Mutex::new(150));
static ANGLE_VIEW: Lazy<Mutex<f32>> = Lazy::new(|| Mutex::new(f32::consts::PI / 2f32));
static DISTANCE_VIEW: Lazy<Mutex<f32>> = Lazy::new(|| Mutex::new(120f32));


pub struct Ant {
    food: Option<Food>,
    internal_clock: f32,
    /// Point of the midle of the base
    position: Vec2,
    vec_movement: Vec2,
    was_lost: bool,
}


impl Ant {

    /// Setup ants settings (static variable).
    pub fn setup(config: &AntConfig) {
        *SPEED.lock().unwrap() = config.speed();
        *ANGLE_VIEW.lock().unwrap() = config.angle_view();
        *DISTANCE_VIEW.lock().unwrap() = config.distance_view() as f32;
   }

    pub fn new(colony: &Colony) -> Ant {
        let position = Vec2::new(colony.x(), colony.y());
        let start_angle_dir = rand::gen_range(0f32, 2f32 * f32::consts::PI);
        Ant {
            position,
            food: None,
            internal_clock: 0f32,
            vec_movement: Vec2::new(
                f32::cos(start_angle_dir),
                f32::sin(start_angle_dir),
            ) * *SPEED.lock().unwrap() as f32 * 0.1,
            was_lost: false,
        }
    }

    pub fn get_pheromone_type(&self) -> PheromoneType {
        match self.food {
            Some(_) => PheromoneType::Food,
            None => PheromoneType::Colony,
        }
    }

    pub fn get_internal_clock(&self) -> f32 {
        self.internal_clock
    }

    pub fn reset_internal_clock(&mut self) {
        self.internal_clock = 0f32;
    }

    pub fn food(&self) -> &Option<Food> {
        &self.food
    }

    pub fn set_food(&mut self, food: Food) {
        self.food = Some(food);
    }

    pub fn unset_food(&mut self) {
        self.food = None;
    }

    pub fn is_lost(&self) -> bool {
        Pheromone::calcul_intensity(
            self.internal_clock, self.get_pheromone_type()) <= 2 
    }

    fn inverse_direction(&mut self) {
        self.vec_movement = -self.vec_movement;
    }

    fn update_position(&mut self) {
        self.position.x += self.vec_movement.x;
        self.position.y += self.vec_movement.y;

        let screen_w = screen_width();
        let screen_h = screen_height();

        if self.position.x > screen_w {
            self.position.x = screen_w;
            self.vec_movement.x = -self.vec_movement.x;
        } else if self.position.x < 0f32 {
            self.position.x = 0f32;
            self.vec_movement.x = -self.vec_movement.x;
        }

        if self.position.y > screen_h {
            self.position.y = screen_h;
            self.vec_movement.y = -self.vec_movement.y;
        } else if self.position.y < 0f32 {
            self.position.y = 0f32;
            self.vec_movement.y = -self.vec_movement.y;
        }
    }

    fn update_clock(&mut self, map: &Map) {
        if let Some(colony) = &map.colony {
            let relative_pos = Vec2::new(
                    colony.borrow().x() - self.position.x,
                    colony.borrow().y() - self.position.y,
                );
                if relative_pos.length() < Colony::radius() * 2. {
                    self.reset_internal_clock()
                }
        }
    }

    fn entity_in_view<E>(&self, entity: &RefCell<E>) -> bool
    where
        E: Updateable + Drawable + Entity<E>
    {
        let left_border_angle: f32 = *ANGLE_VIEW.lock().unwrap() / 2f32;
        let right_border_angle: f32 = - *ANGLE_VIEW.lock().unwrap() / 2f32;
        let relative_pos: Vec2 = Vec2::new(
            entity.borrow().x() - self.position.x,
            entity.borrow().y() - self.position.y
        );

        
        relative_pos.length() <= *DISTANCE_VIEW.lock().unwrap()
            && left_border_angle >= self.vec_movement.angle_between(relative_pos)
            && right_border_angle <= self.vec_movement.angle_between(relative_pos)
    }

    fn random_move(&self) -> Vec2 {
        if rand::gen_range(0, 100) > 97 {
            let dir_angle: f32 = rand::gen_range(0., 2. * f32::consts::PI);
            Vec2::new(f32::cos(dir_angle), f32::sin(dir_angle))
        } else {
            self.vec_movement.normalize()
        }
    }

    fn search_pheromones(&self,
                         pheromones: &Vec<RefCell<Pheromone>>,
                         phero_type: PheromoneType) -> Option<Vec2> {
        let selected_pheromone: Option<&RefCell<Pheromone>> = pheromones
            .iter()
            .filter(
                |pheromone|
                    pheromone.borrow().get_type() == phero_type
                        && self.entity_in_view(pheromone))
            .max_by_key(|pheromone| pheromone.borrow().get_intensity());

        match selected_pheromone {
            Some(pheromone) => Some(
                Vec2::new(
                    pheromone.borrow().x() - self.position.x,
                    pheromone.borrow().y() - self.position.y
                ).normalize()
            ),
            None => None,
        }
    }

    fn search_foods(&self, foods: &Vec<RefCell<Food>>) -> Option<Vec2> {
        let selected_food: Option<&RefCell<Food>> = foods
            .iter()
            .filter(|food| self.entity_in_view(food))
            .min_by_key(
                |food|
                f32::sqrt(
                    f32::powi(self.x() - self.x(), 2)
                    + f32::powi(food.borrow().y() - self.y(), 2)
                ) as u32
            );
        if let Some(food) = selected_food {
            Some(Vec2::new(
                    food.borrow().x() - self.position.x,
                    food.borrow().y() - self.position.y,
                    ).normalize())
        } else {None}
    }

    fn search_colony(&self, map: &Map) -> Option<Vec2> {
        // I don't successfull to compile code with `RefCell<Colony>` in parameter or `&Colony` or
        // `Ref<Colony>`, so I pass the map but it's probably not the better way.
        if let Some(colony) = &map.colony {
            let relative_pos = Vec2::new(
                colony.borrow().x() - self.position.x,
                colony.borrow().y() - self.position.y,
            );
            if relative_pos.length() < Colony::radius() * 2.5 {
                Some(relative_pos.normalize())
            } else {
                None
            }
        } else {None}
    }

    fn research_food_algorithm(&mut self, map: &Map) -> Vec2 {
        match self.search_foods(&map.foods) {
            Some(direction) => direction,
            None => match self.search_pheromones(&map.pheromones.borrow(),
                                                 PheromoneType::Food) {
                Some(direction) => direction,
                None => self.random_move(),
            },
        }
    }

    fn research_colony_algorithm(&mut self, map: &Map) -> Vec2 {
        match self.search_colony(map) {
            Some(direction) => direction,
            None => {
                match self.search_pheromones(&map.pheromones.borrow(),
                                             PheromoneType::Colony) {
                    Some(direction) => direction,
                    None => self.random_move(),
                }
            },
        }
    }

    fn lost_algorithm(&mut self, map: &Map) -> Vec2 {
        if !self.was_lost || rand::gen_range(0, 100) > 97 {
            self.was_lost = true;
            if let Some(colony) = &map.colony {
                let relative_pos = Vec2::new(
                    colony.borrow().x() - self.position.x,
                    colony.borrow().y() - self.position.y,
                );
                let random_angle = rand::gen_range(-f32::consts::PI / 4., f32::consts::PI / 4.);
                let angle_dir = Vec2::X.angle_between(relative_pos) + random_angle;
                Vec2::new(f32::cos(angle_dir), f32::sin(angle_dir))
            } else {
                unreachable!()
            }
        } else {
            self.vec_movement.normalize()
        }
    }

    pub fn reached_colony(&mut self, colony: &Option<RefCell<Colony>>) {
        if let Some(colony) = colony {
            let distance_colony = f32::sqrt(
                f32::powi(colony.borrow().x() - self.position.x, 2)
                + f32::powi(colony.borrow().y() - self.position.y, 2)
            );
            if distance_colony <= COLONY_SIZE / 2f32 {
                self.unset_food();
                colony.borrow_mut().colected_foods += 1;
                self.reset_internal_clock();
                self.inverse_direction();
            }
        }
    }

    pub fn reached_food(&mut self, foods: &mut Vec<RefCell<Food>>) {
        if let Some(found_food) = foods
            .iter()
            .filter(|food| {
                Vec2::new(
                    food.borrow().x() - self.position.x,
                    food.borrow().y() - self.position.y,
                ).length() < ANT_WIDTH / 2f32
            }).min_by_key(|food| {
                Vec2::new(
                    food.borrow().x() - self.position.x,
                    food.borrow().y() - self.position.y,
                ).length() as u32
            })
        {
            let index = foods
                .iter()
                .position(|food| food == found_food)
                .unwrap();
            self.set_food(
                *foods.swap_remove(index).borrow());
            self.reset_internal_clock();
            self.inverse_direction();
        }
    }
}

impl Entity<Self> for Ant {
    fn x(&self) -> f32 {
        self.position.x
    }

    fn y(&self) -> f32 {
        self.position.y
    }
}

impl Drawable for Ant {
    fn draw(&self) {
        let angle_dir_base = Vec2::X
            .angle_between(self.vec_movement) + f32::consts::PI / 2.;
        let dir_base = Vec2::new(
            f32::cos(angle_dir_base),
            f32::sin(angle_dir_base)
        );
        let point_base = Vec2::new(self.position.x, self.position.y);

        draw_triangle(
            point_base + dir_base * (ANT_HEIGHT / 2f32),
            point_base + dir_base * (- ANT_HEIGHT / 2f32),
            point_base + self.vec_movement.normalize() * ANT_WIDTH,
            ANT_COLOR
        );
    }
}

impl Updateable for Ant {
    fn update(&mut self, time_delta: f32, map: &Map, _config: &Config) {
        self.internal_clock += time_delta;
        self.vec_movement = if self.is_lost() {
            self.lost_algorithm(map)
        } else {
            self.was_lost = false;
            match self.food {
                Some(_) => self.research_colony_algorithm(map),
                None => self.research_food_algorithm(map),
            }
        } * *SPEED.lock().unwrap() as f32 * time_delta;
        self.update_position();
        self.update_clock(map);
    }
}

