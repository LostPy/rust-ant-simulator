use macroquad::{shapes::draw_circle, math::Vec2};

use crate::constants::{FOOD_SIZE, FOOD_COLOR};

use super::{Entity, Drawable, Updateable};


#[derive(Debug, Clone, Copy)]
pub struct Food {
    /// Position of center.
    position: Vec2,
}

impl Food {
    pub fn new(x: f32, y: f32) -> Self {
        Self {position: Vec2::new(x, y)}
    }

    pub fn size() -> f32 {
        FOOD_SIZE
    }

    pub fn radius() -> f32 {
        FOOD_SIZE / 2.
    }
}


impl Entity<Self> for Food {
    fn x(&self) -> f32 {
        self.position.x
    }

    fn y(&self) -> f32 {
        self.position.y
    }
}

impl Drawable for Food {
    fn draw(&self) {
        draw_circle(self.position.x, self.position.y, Food::radius(), FOOD_COLOR);
    }
}

impl Updateable for Food {}

impl PartialEq for Food {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::eq(self, other)
    }
}
