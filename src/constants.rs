use std::f32;
use macroquad::prelude::{Color, RED, WHITE, GREEN, BLUE, ORANGE};

/// Define all constants used in simulation.

// Dimensional parameters
pub const PHEROMONE_SIZE: f32 = 6.;
pub const COLONY_SIZE: f32 = 100.;
pub const ANT_WIDTH: f32 = 20.;
pub const ANT_HEIGHT: f32 = 10.;
pub const FOOD_SIZE: f32 = 10.;

// Color parameters
pub const COLONY_COLOR: Color = ORANGE;
pub const ANT_COLOR: Color = WHITE;
pub const FOOD_COLOR: Color = GREEN;
pub const PHEROMONE_COLORS: (Color, Color) = (BLUE, RED);
pub const TEXT_COLOR: Color = WHITE;
