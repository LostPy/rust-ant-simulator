//! Library use for Ant Colony Simulation

pub mod constants;
pub mod config;
pub mod errors;
pub mod event;
pub mod entities;
mod simulation;


pub use simulation::{Simulation, SimulationState};
pub use config::Config;

